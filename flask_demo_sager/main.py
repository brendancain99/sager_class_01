import datetime
from flask import Flask, render_template, request
app = Flask(__name__)


@app.route("/")
@app.route("/test")
def hello():
    return "Hello Mr. Sager's Class!"


@app.route("/page2")
def second_page():
    return render_template('page2.html')


@app.route("/page3")
def third_page():
    my_time = datetime.datetime.now()
    return render_template('page3.html', my_time=my_time)


@app.route("/zip_update",methods=['POST', 'GET'])
def zip_update():
    if request.method == "POST":
        zipcode = request.form["new_zipcode"]
        with open('/home/pi/Desktop/zipcode.txt', 'w') as outfile:
            outfile.write(zipcode)
            outfile.close()
    else:
        with open('/home/pi/Desktop/zipcode.txt', 'r') as infile:
            zipcode = infile.read()

    return render_template('zipcode.html', zipcode=zipcode)


#THIS IS FOR DEBUG ONLY
if __name__ == "__main__":
        app.run(host='0.0.0.0',debug=True)

#Should start service with the following command for actual use
#gunicorn --pythonpath /usr/bin/python3.4  --bind 0.0.0.0:8000 wsgi:app

