from subprocess import call

while(True):
    choice = input("Enter Choice: ")

    if choice == "1":
        print("You chose START MAGIC MIRROR!")
        #stops any running job
        call(["screen","-X","-S","current_job","quit"])
        #starts the new job
        call(["screen","-dmS","current_job","bash","/home/pi/Desktop/start_magic_mirror.sh"])


    elif choice == "2":
        print("You chose Do somthing else!")
        #Stops any jub running
        call(["screen","-X","-S","current_job","quit"])
        #starts the new job
        call(["screen","-dmS","current_job","bash","/home/pi/Desktop/start_magic_mirror.sh"])


    #Just stops the jobs
    elif choice == "end":
       print("Killing Current Job") 
       call(["screen","-X","-S","current_job","quit"])


    #Exit Script
    elif choice == "x":
       print("GoodBye!")
       exit()
